#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<vector>
#include <math.h>
using namespace std;

class Patient {
public:
  int numeroPatient, temps, priorite, tempsRestant;


  Patient(); // constructeur sans argument
  Patient(int, int, int, int);
};

Patient::Patient(){
}

Patient::Patient(int numeroPatient, int temps, int priorite, int tempsRestant){
  this->numeroPatient = numeroPatient;
  this->temps = temps;
  this->priorite = priorite;
  this->tempsRestant = tempsRestant;
}


class Fractile {
public:
  int priorite, nbPatients, nbAtemps, nbRetard;
  float fractile;
  Fractile(); // constructeur sans argument
};

Fractile::Fractile(){
  this->nbPatients = 0;
  this->nbAtemps = 0;
  this->nbRetard = 0;
}

bool lireArgs(int &temps, const int &argc, char * argv[]);
bool ouvrirFichier(ifstream &fPatients, char * argv[]);
void classementPatients(ifstream &fPatients, vector<Patient*> *vPatients, int &temps);

void quickSort(vector<Patient*> *vPatients, int debut, int nombrePatients);
void test (vector<Patient*> *vPatients);

void triDesSacrifices(vector<Patient*> *vPatients, const int &tempsTraitement);
void calculFractile(vector<Patient*> vPatients, Fractile fractileTableau[], int tempsTraitement);


int main(int argc, char * argv[]){

  int temps = 5;
  ifstream fPatients;
  vector<Patient*> vPatients;
  Fractile fractileTableau[4];
  float moyenneGeo = 1.0;
  vector<Patient*>::iterator it;
  int i = 0;

  if(!lireArgs(temps, argc, &argv[0]) || !ouvrirFichier(fPatients, &argv[0])){
    return 1;
  }

  classementPatients(fPatients, &vPatients, temps);
  calculFractile(vPatients, fractileTableau, temps);

  for (int i=0; i<4; i++) {
    moyenneGeo *= fractileTableau[i].fractile;
  }

  moyenneGeo = pow(moyenneGeo, 0.25);

  // Affichage des résultats
  cout << "Affichage des resultats" << endl;
  cout << "-------------------------------------" << endl;
  for ( it = vPatients.begin() ; it != vPatients.end(); ++it){
    cout << i + 1 << " " << (*it)->numeroPatient << " " << (*it)->priorite << endl;
    i++;
  }
  cout << "--------" << endl;
  for (i=0; i<4; i++) {
    cout << fractileTableau[i].fractile << endl;
  }
  cout << moyenneGeo << endl;
  cout << "End Time: now!\n" << endl;
  fPatients.close();
  return 0;

}

bool lireArgs(int &temps, const int &argc, char * argv[]){
  bool valide = true;
  if(argc == 3){
    temps = atoi(argv[2]);
    if (temps < 1){
      cerr << "Entree invalide pour le nombre de minutes moyen de traitement du patient!\n";
      valide = false;
    }
  } else if(argc != 2){
    cerr << "Nombre d'arguments entres invalide\n";
    valide = false;
  }
  return valide;
}

bool ouvrirFichier(ifstream &fPatients, char * argv[]){
  bool trouve = true;
  fPatients.open(argv[1], ios::in);
  if(!fPatients.is_open()){
    cerr << "Erreur a l'ouverture du fichier de patients!\n";
    trouve = false;
  }
  return trouve;
}

void classementPatients(ifstream &fPatients, vector<Patient*> *vPatients, int &tempsTraitement){
  int tempsMax[4] = {15, 30, 60, 120};
  int numeroPatient, temps, priorite;
  vector<Patient*>::iterator it;
  while(fPatients >> numeroPatient && fPatients >> temps && fPatients >> priorite){

    Patient *p = new Patient();
    p->numeroPatient = numeroPatient;
    p->temps = temps;
    p->priorite = priorite;
    p->tempsRestant = tempsMax[p->priorite - 2] - p->temps;
    vPatients->push_back(p);
  }

  quickSort(vPatients, 0, vPatients->size());

  for ( it = vPatients->begin() ; it != vPatients->end(); ++it){
    cout << "tempsRestant:  " << (*it)->tempsRestant << " numeroPatient: " << (*it)->numeroPatient << " temps: " << (*it)->temps << " priorite:  " << (*it)->priorite << endl;
  }
    cout << "----------------------------------------------------------------------------------------" << endl;

  triDesSacrifices(vPatients, tempsTraitement);
  for ( it = vPatients->begin() ; it != vPatients->end(); ++it){
    cout << "tempsRestant:  " << (*it)->tempsRestant << " numeroPatient: " << (*it)->numeroPatient << " temps: " << (*it)->temps << " priorite:  " << (*it)->priorite << endl;
  }
}

void test (vector<Patient*> *vPatients){
  swap((*vPatients)[0], (*vPatients)[1]);

}


void quickSort(vector<Patient*> *vPatients, int debut, int nombrePatients) {
  if(nombrePatients <= 1){
    return;
  }
  int pivot = nombrePatients / 2;
  swap((*vPatients)[debut],(*vPatients)[debut + pivot]);
  int k = 0;
  for(int i = 1; i < nombrePatients; i++){
    if((*vPatients)[debut + i]->tempsRestant < (*vPatients)[debut]->tempsRestant){
      ++k;
      swap((*vPatients)[debut + k],(*vPatients)[debut + i]);
    }
  }
  swap((*vPatients)[debut],(*vPatients)[debut + k]);
  quickSort(vPatients, debut, k);
  quickSort(vPatients, debut+k+1, nombrePatients-k-1);
}

void triDesSacrifices(vector<Patient*> *vPatients, const int &tempsTraitement){
  int tempsPerdu = 0;
  unsigned int it = 0;
  int popCount = 0;
  while(it != (*vPatients).size()-popCount){
    if(tempsPerdu > (*vPatients)[it]->tempsRestant){
      it = it;
      cout << "dead" << (*vPatients)[it]->tempsRestant << endl;
      (*vPatients).push_back((*vPatients)[it]);
      (*vPatients).erase((*vPatients).begin()+it); // pourrait etre facilements optimise.
      ++popCount;
    }else{
      ++it;
      tempsPerdu+=tempsTraitement;
    }
  }
}

void calculFractile(vector<Patient*> vPatients, Fractile fractileTableau[], int temps) {

  vector<Patient*>::iterator it;
  int tempsPasse = 0;

  // Pour chacune des priorité on calcule le nombre de patient + nombre a temps/retard
  for (it = vPatients.begin() ; it != vPatients.end(); ++it){

    fractileTableau[(*it)->priorite - 2].nbPatients++;
    cout << "Temps passe : " << tempsPasse << " Temps restant : " << (*it)->tempsRestant << endl;
    if ((*it)->tempsRestant - tempsPasse >= 0) {
      fractileTableau[(*it)->priorite - 2].nbAtemps++;
    } else {
      fractileTableau[(*it)->priorite - 2].nbRetard++;
    }

    tempsPasse += temps;
  }

  // calcul du fractile
  for (int i = 0; i < 4; i++) {
    // On met la priorité dans le tableau
    fractileTableau[i].priorite = i + 2;
    // On calcule le fractile
    if (fractileTableau[i].nbPatients > 0) {
      fractileTableau[i].fractile = (float) fractileTableau[i].nbAtemps / (float) fractileTableau[i].nbPatients;
    } else {
      fractileTableau[i].fractile = 0;
    }

    // On affiche les lignes
    cout << "Priorite : " << fractileTableau[i].priorite << " NbPatients : " << fractileTableau[i].nbPatients << " A temps : " << fractileTableau[i].nbAtemps << " En retard : " << fractileTableau[i].nbRetard << " Fractile : " << fractileTableau[i].fractile << endl;
  }


}
